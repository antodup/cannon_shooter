﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
	public static float timeLeft = 65.0f;
	public Text timerText;
	private bool timerIsActive = true;
	public int currentScore = 0;
	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {

		if (timerIsActive) {
			timeLeft -= Time.deltaTime;
			timerText.text = "Time Left:" + Mathf.Round (timeLeft);

			if (timeLeft <= 0) {
				timeLeft = 0;
				timerIsActive = false;
			}


		}

		GameObject.Find ("ScoreLabel").GetComponent<Text>().text = "Score:" + currentScore;
	}

}
