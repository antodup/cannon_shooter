﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CannonShooting : MonoBehaviour {
	public int m_PlayerNumber = 1;
	public Rigidbody m_Ball;
	public Transform m_FireTransform;
	public Slider m_AimSlider;
	public float m_MinLaunchForce = 15f; 
	public float m_MaxLaunchForce = 30f; 
	public float m_MaxChargeTime = 0.75f;

	//CannonControl Code
	//public float ShootForce;
	public Material[] mat;
	public Renderer CanonRenderer;
	//CannonControl Code


	private string m_FireButton;         
	private float m_CurrentLaunchForce;  
	private float m_ChargeSpeed;         
	private bool m_Fired;                 

	private void OnEnable()
	{
		m_CurrentLaunchForce = m_MinLaunchForce;
		m_AimSlider.value = m_MinLaunchForce;
	}
	// Use this for initialization
	void Start () {
		ChangeColor ();
		m_FireButton = "Fire" + m_PlayerNumber;

		m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
	}
	
	// Update is called once per frame
	void Update () {
		m_AimSlider.value = m_MinLaunchForce;

		if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired)

		{
			m_CurrentLaunchForce = m_MaxLaunchForce;
			Fire();
			ChangeColor ();
		}
		else if (Input.GetButtonDown(m_FireButton))

		{
			m_Fired = false;
			m_CurrentLaunchForce = m_MinLaunchForce;


		}
		else if (Input.GetButton(m_FireButton) && !m_Fired)

		{
			m_CurrentLaunchForce += m_ChargeSpeed * Time.deltaTime;
			m_AimSlider.value = m_CurrentLaunchForce;
		}
		else if (Input.GetButtonUp(m_FireButton) && !m_Fired)

		{
			Fire();
			ChangeColor ();
		}
		
	}

	private void Fire()
	{
	// Instantiate and launch the ball.
	m_Fired = true;
	Rigidbody shellInstance = Instantiate (m_Ball, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

		//Sends Rigidbody in the Y axis
		shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.up; 
		//changes the colors of the balls
		shellInstance.GetComponent<Renderer>().material = CanonRenderer.material;
		//m_ShootingAudio.clip = m_FireClip;
	m_CurrentLaunchForce = m_MinLaunchForce;
		GetComponent<AudioSource> ().Play();
	}

	//CannonControl Code
	public void ChangeColor() 
	{
		int chosenmaterial = Random.Range (0, mat.Length);
		CanonRenderer.material = mat [chosenmaterial];

	}
	//CannonControl Code
}